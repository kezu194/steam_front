/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function display_dropdown(event) {
    

    let list_dropdown = document.getElementsByClassName("show");
    // If the clicked dropdown is already open, we close it
    if (list_dropdown[0] !== undefined && event.parentNode.children[1] === list_dropdown[0]) {
        list_dropdown[0].classList.remove("show");
    } 
    // Different dropdown, we toggle them
    else {
        if (list_dropdown[0] !== undefined) {
            list_dropdown[0].classList.remove("show");
        }
        event.parentNode.children[1].classList.toggle("show");
    }
}
  
// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropdown-button')) {
        var dropdowns = document.getElementsByClassName("dropdown");
        for (let i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                 openDropdown.classList.remove('show');
            }
        }
    }
} 